#!/usr/bin/env python
# coding: utf-8
import pandas as pd
import tensorflow as tf
import tensorflow_datasets as tfds
mnistData, mnistInfo = tfds.load("mnist",with_info = True, as_supervised = True)
mnistInfo.splits['test']
mnistTrain, mnistTest = mnistData['train'],mnistData['test']
noOfTrainingSamples = mnistInfo.splits['train'].num_examples
noOfValidationSamples = 0.1 * noOfTrainingSamples
noOfValidationSamples = tf.cast(noOfValidationSamples,tf.int64)
noOfTestingSamples = mnistInfo.splits['test'].num_examples
noOfTestingSamples = tf.cast(noOfTestingSamples,tf.int64)
def scale(image,label):
    image = tf.cast(image,tf.float32)
    image /= 255.
    return image,label
scaledTrainAndValidationData = mnistTrain.map(scale)
scaledTestingData = mnistTest.map(scale)
bufferSize = 1000
shuffuledTrainAndValidationData = scaledTrainAndValidationData.shuffle(bufferSize)
shuffuledTestingData = scaledTestingData.shuffle(bufferSize)
validationData = shuffuledTrainAndValidationData.take(noOfValidationSamples)
trainData = shuffuledTrainAndValidationData.skip(noOfValidationSamples)
batchSize = 100
trainData = trainData.batch(batchSize)
validationData = validationData.batch(noOfValidationSamples)
testData = shuffuledTestingData.batch(noOfTestingSamples)
validationDataInputs, validationDataTargets = next(iter(validationData))
inputSize = 784
outputSize = 10
hiddenLayerSize = 200
model = tf.keras.Sequential([
            tf.keras.layers.Flatten(input_shape = (28,28,1)),
            tf.keras.layers.Dense(hiddenLayerSize,activation = 'relu'),
            tf.keras.layers.Dense(hiddenLayerSize,activation = 'relu'),
            tf.keras.layers.Dense(outputSize,activation = 'softmax')
])
model.compile(optimizer='adam',loss='sparse_categorical_crossentropy',metrics=['accuracy'])
noOfEpochs = 10
model.fit(trainData,epochs=noOfEpochs,validation_data=(validationDataInputs, validationDataTargets),verbose=2,validation_steps=3)
testLoss, testAccuracy = model.evaluate(testData)



